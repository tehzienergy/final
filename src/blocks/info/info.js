$(window).on('load resize', function() {

  var sliderLength = 3;
  var slider = $('.info__slider');

  if ($(window).width() >= 992)  {
    slider.removeClass('info__slider--static');
    $('.info__slider.slick-initialized').slick('unslick');

    if ($('.info .card').length > sliderLength) {

      slider.slick({
        slidesToShow: sliderLength,
        slidesToScroll: 1,
        centerMode: true
      });
    }
    
    else {
      slider.addClass('info__slider--static');

      slider.slick({
        slidesToShow: sliderLength,
        slidesToScroll: 1,
        centerMode: true
      });
    }
  }
  
  else {
    slider.removeClass('info__slider--static');
    $('.info__slider.slick-initialized').slick('unslick');

    slider.slick({
      slidesToScroll: 1,
      centerMode: true,
      slidesToShow: 1,
      arrows: false,
      dots: true,
      adaptiveHeight: true
    });
  }
});
