ScrollReveal().reveal('.front__picture-item:nth-child(1)', {
  distance: '50%',
  duration: '2000',
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  opacity: 0,
  origin: 'left',
  viewFactor: .2,
  scale: '1',
  mobile: true,
  reset: false
});

ScrollReveal().reveal('.front__picture-item:nth-child(2)', {
  distance: '50%',
  duration: '2000',
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  opacity: 0,
  origin: 'right',
  viewFactor: .2,
  scale: '1',
  mobile: true,
  reset: false
});

ScrollReveal().reveal('.front__picture-item:nth-child(3)', {
  distance: '50%',
  duration: '2000',
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  opacity: 0,
  delay: 500,
  origin: 'left',
  viewFactor: .2,
  scale: '1',
  mobile: true,
  reset: false
});

ScrollReveal().reveal('.front__picture-item:nth-child(4)', {
  distance: '0',
  duration: '2000',
  rotate: {
    x: 100,
  },
  delay: 1000,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  opacity: 0,
  viewFactor: .2,
  scale: '1',
  mobile: true,
  reset: false
});

ScrollReveal().reveal('.front__picture-item:nth-child(5)', {
  distance: '0',
  duration: '2000',
  rotate: {
    x: 100,
  },
  delay: 1000,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  opacity: 0,
  viewFactor: .2,
  scale: '1',
  mobile: true,
  reset: false
});

ScrollReveal().reveal('.front__picture-item:nth-child(6)', {
  distance: '20%',
  duration: '2000',
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  opacity: 0,
  origin: 'right',
  delay: 1200,
  viewFactor: .2,
  scale: '1',
  mobile: true,
  reset: false
});

ScrollReveal().reveal('.front__picture-item:nth-child(7)', {
  distance: '0',
  duration: '2000',
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  opacity: 0,
  origin: 'right',
  viewFactor: .2,
  delay: 1400,
  scale: '1',
  mobile: true,
  reset: false
});

ScrollReveal().reveal('.front__picture-item--cup', {
  distance: '0',
  duration: '2000',
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  opacity: 0,
  origin: 'right',
  viewFactor: .2,
  delay: 1600,
  scale: '1',
  mobile: true,
  reset: false
});
