$('.menu__btn').click(function(e) {
  e.preventDefault();
  $('body').toggleClass('fixed');
  $(this).toggleClass('menu__btn--active');
  $('.menu__content').toggleClass('menu__content--active');
})